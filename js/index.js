$(function(){

    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval : 2000
    });

    $('#contacto').on('show.bs.modal', function(e){
        console.info("el modal se esta mostrando");
        
        $('.btn-outline-success').addClass('btn-danger');
        $('.btn-outline-success').removeClass('btn-outline-success');
        
        $('.btn-outline-success').prop("disabled", true);
    });

    $('#contacto').on('shown.bs.modal', function(e){
        console.info('el modal se mostro');
    });

    $('#contacto').on('hide.bs.modal', function(e){
        console.info("el modal se esta ocultando");
        
        $('.btn-danger').addClass('btn-outline-success');
        $('.btn-danger').removeClass('btn-danger');

        $('.btn-outline-success').prop("disabled", false);
    })

    $('#contacto').on('hidden.bs.modal', function(e){
        console.info('el modal se oculto');
    });

});